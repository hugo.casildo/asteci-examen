import { HttpClient } from '@angular/common/http';
import { Injectable } from "@angular/core";
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()
export class ApiService{

  constructor (
    private http: HttpClient
  ) {

  }

  getConditions(): Observable<any> {

    return this.http.get('https://api.datos.gob.mx/v1/condiciones-atmosfericas')
      .pipe(map(data => data));

  }

}
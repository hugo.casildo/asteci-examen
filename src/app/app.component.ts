import { Component, OnInit } from '@angular/core';
import { ApiService } from './http/api.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.sass']
})
export class AppComponent implements OnInit {

  title = 'Examen de Hugo Casildo';
  temperaturasResponse = [];
  temperaturas: any;
  pagination = [];
  currentPage = 1;

  constructor(
    private api: ApiService
  ){}

  ngOnInit(): void {
    let arrayTemp = [];
    this.api.getConditions()
      .subscribe(data => {
        this.temperaturasResponse = data.results;
        this.fillPagination(this.getNumberOfPages(this.temperaturasResponse.length));
        arrayTemp = this.temperaturasResponse.slice();
        this.temperaturas = arrayTemp.slice(0, 10);
      })
  }

  showDetail(index) {
    if ( this.temperaturas[index].show ){
      this.temperaturas[index].show = !this.temperaturas[index].show;
    } else {
      this.temperaturas[index].show = true;
    }
  }

  getNumberOfPages(size){
    return ( Math.trunc( size / 10 )  ) + ( ( size % 10 > 0 ) ? 1 : 0  );
  }

  probability(precip, hum) {
    return (precip > 60 || hum > 50) ? 'Si' : 'No';
  }

  fillPagination(max) {
    for (let x = 0; x < max; x++){
      this.pagination.push(x +1);
    }
  }

  changeTab(index) {
    const start = (index - 1) * 10;
    const end = index * 10
    const arrayTemp = this.temperaturasResponse.slice();
    this.currentPage = index;
    this.temperaturas = [];
    this.temperaturas = arrayTemp.slice(start, end);
  }
}
